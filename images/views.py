from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .models import Image
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


@method_decorator(login_required, name='dispatch')
class ImageList(ListView):
    #permission_required = 'images.view_image'
    model = Image
    template_name = 'images/images.html'
    context_object_name = 'image_list'
    paginate_by = 16


class ImageDetail(DetailView):
    #permission_required = ['images.view_image', 'images.delete_image']
    model = Image
    template_name = 'images/image_detail.html'
    slug_field = 'slug'


# @login_required
# def image_create(request):
#     if request.method == 'POST':
#         form = CreateImage(data=request.POST)
#         if form.is_valid():
#             new_image = form.save(commit=False)
#             new_image.user = request.user
#             new_image.save()
#             messages.success(request, 'Image added succesfully')
#             return render(request, 'images/image_detail.html')
#     else:
#         form = CreateImage(data=request.GET)
#     return render(request, 'images/image_form.html', {'form': form})
@method_decorator(login_required, name='dispatch')
class ImageCreate(CreateView):
    #permission_required = 'images.add_image'
    model = Image
    fields = ['title', 'picture', 'slug']
    success_url = '/gallery/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class ImageDelete(DeleteView):
    #permission_required = 'images.delete_image'
    model = Image
    success_url = '/gallery/'
    template_name = 'images/image_confirm_delete.html'

