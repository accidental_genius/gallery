# Generated by Django 4.1.7 on 2023-03-11 17:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0005_alter_image_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='slug',
            field=models.SlugField(default='', max_length=200),
        ),
    ]
