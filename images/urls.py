from django.urls import path
from images.views import ImageList, ImageCreate, ImageDetail, ImageDelete

app_name = 'images'

urlpatterns = [
    path('', ImageList.as_view(), name='images-list'),
    path('image/create/', ImageCreate.as_view(), name='images-create'),
    path('image/detail/<slug:slug>/', ImageDetail.as_view(), name='images_detail'),
    path('image/delete/<int:pk>/', ImageDelete.as_view(), name='images_delete')
]

