from django.db import models
from django.contrib.auth.models import User


class Image(models.Model):
    title = models.CharField(max_length=150)
    # tags = models.CharField(max_length=200)
    picture = models.ImageField(upload_to='pictures/%Y/%m/%d/', max_length=200, null=True)
    slug = models.SlugField(max_length=200, default='')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='image_content')

    class Meta:
        indexes = [
            models.Index(fields=['-created']),
        ]
        ordering = ['-created']

    def __str__(self):
        return self.title
