from django import forms
from .models import Audio


class CreateAudio(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ['title', 'tags', 'audio']

