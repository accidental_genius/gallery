from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView

from gallery.audios.models import Audio


@method_decorator(login_required, name='dispatch')
class AudioList(PermissionRequiredMixin, ListView):
    #permission_required = 'images'
    model = Audio
    template_name = 'audios/audios.html'
    context_object_name = 'audio_list'
    paginate_by = 12


class AudioDetail(DetailView):
    model = Audio

