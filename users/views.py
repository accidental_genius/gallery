from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
# from django.contrib.auth.models import Permission, Group
from .forms import LoginForm, RegistrationForm
# from images.models import Image
# from videos.models import Video
# from audios.models import Audio

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request,
                                username=cd['username'],
                                password=cd['password'])
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('images-list'))
        else:
            return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'registration/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return render(request, 'registration/logged_out.html')


def register(request):
    if request.method == 'POST':
        user_form = RegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(
                user_form.cleaned_data['password'])
            new_user.save()
            # author_group, created = Group.objects.get_or_create(name="Author")
            # content_type = ContentType.objects.get_for_models(Image, Video, Audio)
            # print(content_type)
            # permission = Permission.objects.filter(content_type=content_type)
            # print(permission)
            # for perm in permission:
            #     author_group.permissions.add(perm)
            # new_user.groups.add(author_group)
            # #new_user.user_permissions.add(['images.add_image', 'images.view_image', 'images.change_image', 'images.delete_image',
            #                               #  'videos.add_video', 'videos.view_video', 'videos.change_video', 'videos.delete_video',
            #                               # 'audios.add_audio', 'audios.view_audio', 'audios.change_audio', 'audios.delete_audio'])
            return render(request,
                          'registration/register_done.html',
                          {'new_user': new_user})
    else:
        user_form = RegistrationForm()
    return render(request, 'registration/register.html', {'user_form': user_form})
