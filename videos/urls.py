from django.urls import path
from videos.views import VideoList, VideoCreate, VideoDetail, VideoDelete

app_name = 'videos'

urlpatterns = [
    path('', VideoList.as_view(), name='videos-list'),
    path('create/', VideoCreate.as_view(), name='videos-create'),
    path('detail/<slug:slug>/', VideoDetail.as_view(), name='videos_detail'),
    path('delete/<int:pk>/', VideoDelete.as_view(), name='videos_delete')
]