from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator


# class Tag(models.Model):
#     name = models.CharField(max_length=150)
#
#     def __str__(self):
#         return self.name


class Video(models.Model):
    title = models.CharField(max_length=150)
    # tags = models.ManyToManyField(Tag, related_name='gallery_posts')
    video = models.FileField(upload_to='videos/%Y/%m/%d',
                             validators=[
                                 FileExtensionValidator(allowed_extensions=['mp4', 'avi', 'mkv', 'mov', 'wmv', 'gif'])])
    slug = models.SlugField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='video_content')

    class Meta:
        indexes = [
            models.Index(fields=['-created']),
        ]
        ordering = ['-created']

    def __str__(self):
        return self.title



