from django.contrib.auth.decorators import login_required
# from django.contrib.auth.mixins import PermissionRequiredMixin
# from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .models import Video


#@method_decorator(login_required, name='dispatch')
class VideoList(ListView):
    #permission_required = ['videos.add_video', 'videos.view_video', 'videos.change_video', 'videos.delete_video']
    model = Video
    template_name = 'videos/video_list.html'
    context_object_name = 'video_list'
    paginate_by = 16


class VideoDetail(DetailView):
    model = Video
    template_name = 'videos/video_detail.html'
    slug_field = 'slug'


@method_decorator(login_required, name='dispatch')
class VideoCreate(CreateView):
    model = Video
    fields = ['title', 'video', 'slug']
    success_url = '/gallery/'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class VideoDelete(DeleteView):
    model = Video
    success_url = '/gallery/'
    template_name = 'videos/video_confirm_delete.html'
